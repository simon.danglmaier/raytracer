# Raytracer

## Info

Idea and code taken from https://raytracing.github.io/ .

My implementation is basically the same as this book, i just use different naming convention for my variables, used
more `constexpr` and modern C++ functionality and i made the project structure unnecessarily difficult, to get the hang of CMake.

Up to now i have implemented the code and functionality from Book 1, but i plan to add improvements and the code from the other books as well.

